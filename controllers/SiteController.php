<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Etapa;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
     
    //QUERY 1 DAO
   public function actionConsulta1(){
        $dataProvider = new SqlDataProvider(['sql'=>'SELECT DISTINCT COUNT(dorsal) as total FROM ciclista'

        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>" Número de ciclistas que hay",
            "sql"=>"SELECT DISTINCT COUNT(dorsal) as total FROM ciclista",
        ]);
     }
     /*Definicion de la consulta 1
      * Utilizamos la funcion de totales count para que nos sume todos los dorsales que hay en la tabla de ciclista 
      */
     //QUERY 1 DAO
    public function actionConsulta1a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Ciclista::find()
                ->select("COUNT(dorsal) as total")
                ->distinct(),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 1 con Action Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT DISTINCT COUNT(dorsal) as total FROM ciclista",
        ]);
}
    
      
    //QUERY 2 DAO
    public function actionConsulta2(){
        
        $dataProvider = new SqlDataProvider([
            
            'sql'=>'SELECT DISTINCT COUNT(dorsal) as totalbanesto FROM ciclista WHERE nomequipo = "Banesto"',
        ]);
        
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['totalbanesto'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>'SELECT DISTINCT COUNT(dorsal)as totalbanesto FROM ciclista WHERE nomequipo = "Banesto"',
        ]);
    }
      /*Definicion consulta 2
      * Con el operador Count hacemos una suma de todos los dorsales aplicando la restricion de que estos dorsales sean 
      * ciclistas del equipo de banesto y le añadimos el alias previamente definido en el modelo.
      */
        
    //QUERY 2 ORM
    public function actionConsulta2a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Ciclista::find()
                ->select("COUNT(dorsal) as totalbanesto")
                ->distinct()
                ->where('nomequipo = "banesto"'),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['totalbanesto'],
            "titulo"=>"Consulta 2 con Action Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(dorsal) as totalbanesto FROM ciclista WHERE nomequipo = 'banesto'",
        ]);
    }
    
       
    //QUERY 3 DAO
    public function actionConsulta3(){
        
        $dataProvider = new SqlDataProvider([
            
            'sql'=>'SELECT DISTINCT AVG (edad) as mediaedad FROM ciclista',
        ]);
        
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['mediaedad'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT DISTINCT AVG (edad) as mediaedad FROM ciclista",
        ]);
    }
      /*Definicion Consulta 3
      * Utilizamos la funcion avg indicando el campo del que queremos hacer la media de la tabla ciclistas y le ponemos un alias al resultado de la media
      * 
      */
           
    //QUERY 3 ORM
    public function actionConsulta3a(){
        $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()
                   ->select("AVG(edad) as mediaedad")
                   ->distinct(),
                   
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['mediaedad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT DISTINCT AVG (edad) as mediaedad FROM ciclista",
        ]);
    }
    //QUERY 4 DAO
     public function actionConsulta4(){
        
        $dataProvider = new SqlDataProvider([
            
            'sql'=>'SELECT DISTINCT AVG (edad) as mediaedadbanesto FROM ciclista WHERE nomequipo = "Banesto"',
        ]);
        
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['mediaedadbanesto'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"La edad media de los ciclistas del equipo Banesto",
            "sql"=>'"SELECT DISTINCT AVG (edad) as mediaedadbanesto FROM ciclista WHERE nomequipo = "Banesto"',
        ]);
    }
      /*Definicion consulta 4
      * Con la funcion avg hacemos la media de la edad llamandola mediaedadbanesto con un alias y en el where aplicamos la resticion de que pertenezcan al equipo banesto
      * 
      */
    //QUERY 4 ORM
    public function actionConsulta4a(){
        $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()
                   ->select("AVG(edad) as mediaedadbanesto")
                   ->distinct()
                   ->where("nomequipo = 'Banesto'"),
                   
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['mediaedadbanesto'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"La edad media de los ciclistas del equipo Banesto",
            "sql"=>"SELECT DISTINCT AVG(edad) as mediaedadbanesto FROM ciclista",
        ]);
    }
    
     //QUERY 5 DAO
    public function actionConsulta5(){
        
        $dataProvider = new SqlDataProvider([
            
            'sql'=>'SELECT AVG (edad) as mediaedadequipos, nomequipo FROM  ciclista GROUP BY nomequipo',
        ]);
        
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['mediaedadequipos','nomequipo'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT AVG (edad) as mediaedadequipos FROM ciclista Group BY nomequipo",
        ]);
    }
    /*Definicion consulta 5
     * 
     * Con el avg hacemos la media de edad de los equipos y los agrupamos por el nombre de equipo
     * 
     */
           
    //QUERY 5 ORM
    public function actionConsulta5a(){
        $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("AVG(edad) as mediaedadequipos, nomequipo")
                ->groupBy("nomequipo"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['mediaedadequipos','nomequipo'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT AVG (edad) as mediaedadequipos, nomequipo FROM  ciclista GROUP BY nomequipo",
        ]);
    }
    //QUERY 6 DAO
    public function actionConsulta6(){
        
        $dataProvider = new SqlDataProvider([
            
            'sql'=>'SELECT COUNT(dorsal) as ciclistasporequipo, nomequipo FROM ciclista GROUP BY nomequipo'
        ]);
        
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','ciclistasporequipo'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>" El número de ciclistas por equipo",
            "sql"=>"SELECT COUNT(dorsal) as ciclistasporequipo,nomequipo FROM ciclista GROUP BY nomequipo",
        ]);
    }
           /*Definicion consulta 6
            * 
            * Contamos los dorsales que hay en total y mostramos el nombre de equipo con el total de dorsales que cuenta
            * 
            */
    //QUERY 6 ORM
    public function actionConsulta6a(){
        $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("COUNT(dorsal) as ciclistasporequipo,nomequipo")
                ->GROUPBY("nomequipo"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','ciclistasporequipo'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>" El número de ciclistas por equipo",
            "sql"=>"SELECT COUNT(dorsal) as ciclistasporequipo,nomequipo FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
     //QUERY 7 DAO
    public function actionConsulta7(){
        
        $dataProvider = new SqlDataProvider([
            
            'sql'=>'SELECT COUNT(nompuerto) AS totalpuertos FROM puerto'
        ]);
        
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['totalpuertos'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(nompuerto) AS totalpuertos",
        ]);
    }
           /*Definicion consulta 7
            * 
            * Contamos el numero total de puertos que hay y los mostramos
            */
    //QUERY 7 ORM
    public function actionConsulta7a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("COUNT(nompuerto) as totalpuertos")
                                     ->distinct(),
        ]);
         return $this->render("resultado", [
            "resultados"=>$dataProvider, 
            "campos"=>['totalpuertos'],
            "titulo"=> "Consulta 7 con ActiveDataProvider ",
            "enunciado" => "El número total de puertos",
            "sql" =>'SELECT COUNT(nompuerto) AS totalpuertos',
        ]);
   }
   
   //QUERY 8 DAO
   public function actionConsulta8(){
        $dataProvider = new SqlDataprovider(['sql'=>'SELECT COUNT(nompuerto) AS totalpuertos FROM puerto WHERE altura > 1500'
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['totalpuertos'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(nompuerto) as totalpuertos FROM puerto WHERE altura > 1500",
                ]);
    }
    /*Definicion consulta 8
     * 
     * Contamos el numero total de puertos que hay y en el where aplicamos la restriccion de que solo nos muestre aquellos que su altura es superior a 1500 metros
     */
    //QUERY 8 ORM
    public function actionConsulta8a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Puerto::find()->select("COUNT(nompuerto) AS totalpuertos")
                                 ->distinct()
                                 ->where("altura > 1500"),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['totalpuertos'],
            "titulo"=>"Consulta 8 con Action Record",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(nompuerto) as totalpuertos FROM puerto WHERE altura > 1500",
        ]);
    }
     //QUERY 9 DAO
  public function actionConsulta9(){
        $dataProvider = new SqlDataprovider(['sql'=>'SELECT nomequipo FROM  ciclista GROUP BY  nomequipo HAVING COUNT(dorsal)>4'
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT COUNT(dorsal),nomequipo FROM  ciclista GROUP BY  nomequipo HAVING COUNT(dorsal)>4",
                ]);
    }
    /*Definicion consulta 9
     * 
     * Agrupamos los ciclistas por sus equipos y en el having aplicamos la restrcion a los grupos de que solo muestren los equipos que cuentan con mas de 4 dorsales
     */
    //QUERY 9 ORM
    public function actionConsulta9a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Ciclista::find()->select("nomequipo")
                                   ->groupBy("nomequipo")
                                   ->having("COUNT(dorsal)>4"),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con Action Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo FROM ciclista GROUP BY  nomequipo HAVING COUNT(dorsal)>4",
        ]);
    }
      //QUERY 10 DAO
    public function actionConsulta10(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo FROM ciclista GROUP BY("nomequipo") Having(count(dorsal>4) AND "edad" BETWEEN 28 AND 32)',
        ]);

        return $this->render("resultado", [
            "resultados"=>$dataProvider, 
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 10 con DAO ",
            "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql" =>'SELECT nomequipo FROM ciclista GROUP BY("nomequipo") Having(count(dorsal>4) AND "edad" BETWEEN 28 AND 32)',
        ]);
    }
    /*Definicion consulta 10
     * 
     * Teniendo agrupados los ciclistas en sus equipos mostramos el nombre de aquellos que cuenten con mas de 4 ciclistas y su edad este entre 28 y 32 años
     */
       //QUERY 10 ORM
       public function actionConsulta10a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("nomequipo")
                ->GROUPBY("nomequipo")
                ->HAVING("COUNT(dorsal)>4 AND 'edad' BETWEEN 28 AND 32"),
        ]);
         return $this->render("resultado", [
            "resultados"=>$dataProvider, 
            "campos"=>['nomequipo'],
            "titulo"=> "Consulta 10 con ActiveDataProvider ",
            "enunciado" =>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql" => 'SELECT nomequipo FROM ciclista GROUP BY("nomequipo") Having(count(dorsal>4) AND "edad" BETWEEN 28 AND 32)',
        ]);
    }
    //QUERY 11 DAO
    public function actionConsulta11(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal,COUNT(dorsal) as etapasganadas FROM Etapa GROUP BY dorsal'
        ]);

        return $this->render("resultado", [
            "resultados"=>$dataProvider, 
            "campos"=>['dorsal','etapasganadas'],
            "titulo"=> "Consulta 11 con DAO ",
            "enunciado" =>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql" =>'SELECT dorsal, COUNT(dorsal) as etapasganadas FROM Etapa GROUP BY dorsal',
        ]);
    }
    /*Definicion consulta 11
     *Con el COUNT sumamos todas las etapas que ha ganado el ciclista y las agrupamos por el numero de dorsal
     * 
     */
      //QUERY 11 ORM
      public function actionConsulta11a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()->select("dorsal, COUNT(dorsal) AS etapasganadas")
                ->GROUPBY("dorsal"),
        ]);
         return $this->render("resultado", [
            "resultados"=>$dataProvider, 
            "campos"=>['dorsal','etapasganadas'],
            "titulo"=> "Consulta 11 con ActiveDataProvider ",
            "enunciado" =>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql" =>'SELECT dorsal, COUNT(dorsal) as etapasganadas FROM Etapa GROUP BY dorsal',
        ]);
    }
    //QUERY 12 DAO
    public function actionConsulta12(){
        $dataProvider = new SqlDataprovider(['sql'=>'SELECT dorsal  FROM etapa GROUP BY dorsal HAVING COUNT(dorsal) > 1'
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal  FROM etapa GROUP BY dorsal HAVING COUNT(dorsal) > 1",
                ]);
    }
    /*Definicion consulta 12
     * 
     * sumamos las etapas que ha ganado cada ciclista y las agrupamos por su numero de dorsal luego aplicamos 
     * la condicion al grupo de que solo muestre los dorsales que han ganado mas de  una etapa
     */
    //QUERY 12 ORM
    public function actionConsulta12a(){
        $dataProvider = new ActiveDataProvider([
         'query'=> Etapa::find()->select("dorsal")->groupBy("dorsal")->having("COUNT(dorsal)>1"),
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con Action Record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal  FROM etapa GROUP BY dorsal HAVING COUNT(dorsal) > 1",
        ]);
    }

}

